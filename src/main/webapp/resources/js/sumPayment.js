$(function() {
	let total = 0;

	let map = new Map();
	let sortMap = [];

	$("#result_table > tbody > tr")
			.each(
					function() {
						if ($(this).attr("class") != "header-row") {

							var key = $(this).children("td")
									.filter(".category").text();

							var value = parseInt($(this).children("td").filter(
									".payment").text());

							total += value;

							if (map.has(key)) {
								map.set(key, map.get(key) + value);
							} else {
								map.set(key, value);
							}
						}
					});

	// 合計行の追加
	var total_row = $("<tr><td colspan=2>合計</td>" + "<td align=right class='payment'>"
			+ total + "</td>" + "<td></td></tr>");
	$("#result_table > tbody > tr:last").after(total_row);

	//
	map.forEach(function(a, b) {
		sortMap.push({key:b ,value:a })
	})

	sortMap.sort(function(a,b){
		if(a.value < b.value) return 1;
		if(a.value > b.value) return -1;
		return 0;
	})


	let keys = new Array();
	let values = new Array();

	sortMap.forEach(function(map){
		keys.push(map.key);
		values.push(map.value);
	})

	var ctx = $("#myPieChart")[0];

	$('#canvas-area').attr('width', '500px');
	$('#canvas-area').attr('height', '500px');


	new Chart(ctx, {
		type : 'pie',
		data : {
			labels : keys,
			datasets : [ {
				data : values
			} ]
		},
		options : {
			title : {
				display : true,
				text : '出費の割合'
			},
			plugins : {
				colorschemes : {
					scheme : 'brewer.Paired12'
				}
			}
		}
	});
}

);