$(function () {
  $('#editModal').on('show.bs.modal', function (event) {
    //モーダルを開いたボタンを取得
    const inputData = $(event.relatedTarget);

    //data-watasuの値取得
    const paymentId = inputData.data('paymentid');
    const paymentDay = inputData.data('paymentday');
    const paymentCategory = inputData.data('paymentcategory');
    const payment = inputData.data('paymentmoney');
    const paymentMemo = inputData.data('paymentmemo');

    //モーダルを取得
    var modal = $(this);
    //受け取った値をspanタグのとこに表示
    modal.find('.modal-body input#id').val(paymentId);
    $('#day option[value='+paymentDay+']').prop('selected',true);
    $('#categoryId option[value='+paymentCategory+']').prop('selected',true);
    modal.find('.modal-body input#payment').val(payment);
    modal.find('.modal-body input#memo').val(paymentMemo);

  });
})

