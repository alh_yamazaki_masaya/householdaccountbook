$(document)
		.ready(
				function() {
					var param = getParam('month');

					var month = param.substring(0, 4) + "年"
							+ param.substring(4, 6) + "月";

					var title = document.getElementById('title')
					title.innerHTML = '<h1>' + month + '</h1>'

					function getParam(name, url) {
						if (!url)
							url = window.location.href;
						name = name.replace(/[\[\]]/g, "\\$&");
						var regex = new RegExp("[?&]" + name
								+ "(=([^&#]*)|&|#|$)"), results = regex
								.exec(url);
						if (!results)
							return null;
						if (!results[2])
							return '';
						return decodeURIComponent(results[2]
								.replace(/\+/g, " "));
					}

				});