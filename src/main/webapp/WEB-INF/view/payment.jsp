<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home画面</title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link rel="stylesheet"	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
<script type="text/javascript"	src="<c:url value="/resources/js/setTitle.js" />"></script>
<script type="text/javascript"	src="<c:url value="/resources/js/sumPayment.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modalForm.js" />"></script>
</head>

</head>
<body>

	<!-- モーダルフォーム -->
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">編集画面</h5>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
				</div><!-- /.modal-header -->
				<div class="modal-body">
					<form:form modelAttribute="paymentForm"
						action="${pageContext.request.contextPath}/payment/edit" >
						<form:hidden path="id"/>
						<form:hidden path="month" value="${month}" />
						<form:label path="day">日付</form:label>
						<form:select class="form-control" path="day" items="${dayList}" />
						<form:label path="categoryId">カテゴリ</form:label>
						<form:select class="form-control" path="categoryId" items="${categories}" />
						<form:label path="payment">金額</form:label>
						<form:input type="number" min="1" class="form-control" path="payment" />
						<form:label path="memo">メモ</form:label>
						<form:input class="form-control" path="memo" />
						<input type="submit" class="btn btn-primary" name="update" value="変更を保存" >
						<button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>

						<input type="submit" class="btn btn-primary" name="delete" value="削除" />
					</form:form>
				</div><!-- /.modal-body -->
				<div class="modal-footer">

				</div><!-- /.modal-footer -->
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->



	<div class="container">


		<div class="header">
			<a class="btn btn-primary" href="${pageContext.request.contextPath}/">戻る</a>
		</div>

		<div id="title"></div>

		<div class="add-payment">
			<form:form modelAttribute="paymentForm"
				action="${pageContext.request.contextPath}/payment/register">
				<form:hidden path="month" value="${month}" />
				<table class="table">
					<thead>
						<tr class="header-row">
							<th>日付</th>
							<th>カテゴリ</th>
							<th>金額</th>
							<th>メモ</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th><form:select class="form-control" path="day" items="${dayList}" /></th>
							<th><form:select class="form-control" path="categoryId" items="${categories}" /></th>
							<th><form:input type="number" min="1" class="form-control" path="payment" /></th>
							<th><form:input class="form-control" path="memo" /></th>
						</tr>
					</tbody>
					<tfoot>
						<tr>
						<td colspan=4 align=right><input type="submit" class="btn btn-default" value="登録" /></td>
					</tfoot>
				</table>
			</form:form>
		</div>

		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#tab1" class="nav-link active" data-toggle="tab" aria-expanded="true">テーブル</a>
			</li>
			<li>
				<a href="#tab2" data-toggle="tab" aria-expanded="false">グラフ</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab1">
				<c:if test="${ not empty payments }">
					<div class="payments_list">
						<div class="clearfix">
							<table id="result_table" class="table table-bordered table-hover">
								<thead>
									<tr class="header-row">
										<td align=center>日付</td>
										<td align=center>カテゴリ</td>
										<td align=center>金額</td>
										<td align=center>メモ</td>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${payments}" var="payment">
										<!-- data-○○の○○の部分を指定して取得できる -->
										<tr id="result-row"  data-toggle="modal" data-target="#editModal"
											data-paymentid="${payment.id}" data-paymentday="${payment.day}"
											data-paymentcategory="${payment.categoryId}" data-paymentmoney="${payment.payment}"
											data-paymentmemo="${payment.memo}" >

											<td align=right>${payment.day}</td>
											<td class="category">${categories[payment.categoryId]}</td>
											<td align=right class="payment">${payment.payment}</td>
											<td>${payment.memo}</td>
											<!-- <td align=center><button type="button" class="btn btn-link" ><i class="glyphicon glyphicon-cog"></i></button></td> -->
										</tr>
									</c:forEach>
								</tbody>
							</table>

						</div>
					</div>
				</c:if>
			</div>
			<div class="tab-pane" id="tab2">
				<div id="canvas-area" style="height: 500px; width: 500px">
					<canvas id="myPieChart"></canvas>
				</div>
			</div>
		</div>
		<div class="copyright">Copyright(c)Yamazaki Masaya</div>

	</div>

	<script src="https://unpkg.com/chartjs-plugin-colorschemes"></script>
</body>
</html>