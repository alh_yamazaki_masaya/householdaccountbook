<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>

<head>
	<meta charset="utf-8">
	<title>Home画面</title>
	<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
	<link rel="stylesheet"	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
	<script>
		$(function() {
			$('li#month-list').click(function(){
				$('<form/>', {id:"paymentForm", action: "${pageContext.request.contextPath}/payment", method: 'get'})
				.append($('<input/>', {type: 'hidden', name: 'month', value: $(this).val()}))
				.appendTo(document.body)
				.submit();
			});
		});
	</script>

	<div class="container">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" /></li>
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<c:if test="${ not empty months }">
			<div class="month_list">
				<ul class="list-group">
					<c:forEach items="${months}" var="month">
						<li class="list-group-item" id="month-list" value="<fmt:formatDate value="${month.month}" pattern="yyyyMM" />">
						<fmt:formatDate value="${month.month }" pattern="yyyy/MM" /><br />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<div class="add-book">
			<form:form modelAttribute="registerForm"
				action="${pageContext.request.contextPath}/regist/month">
				<fieldset>
					<legend>追加</legend>
					<form:input class="form-control"  type="month" path="month"/>
					<input type="submit" value="登録" />
				</fieldset>
			</form:form>
		</div>
		<div class="copyright">Copyright(c)Yamazaki Masaya</div>
	</div>

</body>
</html>