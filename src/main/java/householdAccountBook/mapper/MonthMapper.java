package householdAccountBook.mapper;

import java.util.Date;
import java.util.List;

import householdAccountBook.entity.Month;

public interface MonthMapper {

	void register(Month month);

	List<Month> getMonthist();

	Month getMonth(Date date);
}