package householdAccountBook.mapper;

import java.util.List;

import householdAccountBook.entity.ColumnCategory;

public interface ColumnCategoryMapper {

	List<ColumnCategory> getCategoryAll();

}