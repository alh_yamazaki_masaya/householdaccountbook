package householdAccountBook.mapper;

import java.util.Date;
import java.util.List;

import householdAccountBook.entity.Payment;

public interface PaymentMapper {

	List<Payment> getPaymentAll(Date month);

	void insertPayment(Payment payment);

	void updatePayment(Payment payment);

	void deletePayment(int id);
}