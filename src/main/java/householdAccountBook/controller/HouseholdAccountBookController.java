package householdAccountBook.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import householdAccountBook.dto.MonthDto;
import householdAccountBook.entity.Month;
import householdAccountBook.form.MonthForm;
import householdAccountBook.form.PaymentForm;
import householdAccountBook.service.HomeService;
import householdAccountBook.service.HouseholdAccountBookService;

@Controller
public class HouseholdAccountBookController {

	@Autowired
	HouseholdAccountBookService householdAccountBookService;

	@Autowired
	HomeService homeService;

	@RequestMapping(value = "/regist/month", method = RequestMethod.POST)
	public String regist(@ModelAttribute MonthForm monthForm, Model model) {

		MonthDto dto = new MonthDto();
		convertToDto(dto, monthForm);

		if(!(homeService.getMonth(dto.getMonth())== null)) {
			List<String> errorMessages = new ArrayList<String>();
			String errorMessage = "重複しています";

			errorMessages.add(errorMessage);

			model.addAttribute("errorMessages",errorMessages);


			List<Month> months = homeService.getMonthList();
			PaymentForm paymentForm = new PaymentForm();

			model.addAttribute("months", months);
			model.addAttribute("registerForm", monthForm);
			model.addAttribute("paymentForm", paymentForm);

			return "home";
		}

		householdAccountBookService.register(dto);

		return "redirect:/";
	}



	private void convertToForm(MonthForm form, MonthDto dto) {

		BeanUtils.copyProperties(dto, form);
		if(dto.getMonth() != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
			form.setMonth(simpleDateFormat.format(dto.getMonth()));
		}

	}

	private void convertToDto(MonthDto dto, MonthForm form) {

		BeanUtils.copyProperties(form, dto);

		if (form.getMonth() != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
			try {
				dto.setMonth(simpleDateFormat.parse(form.getMonth()));
			} catch (ParseException e) {
			}
		}

	}


}
