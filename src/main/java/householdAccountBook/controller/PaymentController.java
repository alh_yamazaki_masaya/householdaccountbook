package householdAccountBook.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import householdAccountBook.entity.Payment;
import householdAccountBook.form.PaymentForm;
import householdAccountBook.service.ColumnCategoryService;
import householdAccountBook.service.PaymentService;

@Controller
public class PaymentController {

	@Autowired
	PaymentService paymentService;

	@Autowired
	ColumnCategoryService columnCategoryService;

	@RequestMapping(value = "/payment", method = RequestMethod.GET)
	public String show(@ModelAttribute("paymentForm") PaymentForm paymentForm, Model model) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
		Date month = new Date();

		String formatMonth = paymentForm.getFormatMonth();
		try {
			month = simpleDateFormat.parse(formatMonth);
		} catch (ParseException e) {

		}
		List<Payment>payments = paymentService.getPaymentAll(month);
		List<Integer>dayList = getDayList(month);
		Map<Integer, String> categories = columnCategoryService.getCategoryAll();

		model.addAttribute("payments",payments);
		model.addAttribute("dayList",dayList);
		model.addAttribute("paymentForm", paymentForm);
		model.addAttribute("categories", categories);
		return "payment";
	}

	@RequestMapping(value = "/payment/register", method = RequestMethod.POST)
	public String register(@ModelAttribute PaymentForm paymentForm, Model model) {

		paymentService.insertPayment(paymentForm);


		// PaymentFormの初期化
		String month = paymentForm.getMonth();

		paymentForm = new PaymentForm();
		paymentForm.setMonth(month);

		model.addAttribute("paymentForm",paymentForm);

		return "redirect:/payment?month=" + month;
	}

	@RequestMapping(value = "/payment/edit", params = "update", method = RequestMethod.POST)
	public String update(@ModelAttribute PaymentForm paymentForm, Model model) {



		paymentService.updatePayment(paymentForm);

		// PaymentFormの初期化
		String month = paymentForm.getMonth();

		paymentForm = new PaymentForm();
		paymentForm.setMonth(month);

		model.addAttribute("paymentForm",paymentForm);

		return "redirect:/payment?month=" + month;
	}

	@RequestMapping(value = "/payment/edit", params = "delete", method = RequestMethod.POST)
	public String delete(@ModelAttribute PaymentForm paymentForm, Model model) {

		paymentService.deletePayment(paymentForm);

		// PaymentFormの初期化
		String month = paymentForm.getMonth();

		paymentForm = new PaymentForm();
		paymentForm.setMonth(month);

		model.addAttribute("paymentForm",paymentForm);

		return "redirect:/payment?month=" + month;
	}

	private List<Integer> getDayList(Date yearAndMonth){

		List<Integer>dayList = new ArrayList<Integer>();

		SimpleDateFormat ysdf = new SimpleDateFormat("yyyy");
		SimpleDateFormat msdf = new SimpleDateFormat("MM");


		int year = Integer.parseInt(ysdf.format(yearAndMonth));
		int month = Integer.parseInt(msdf.format(yearAndMonth));

		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month -1,1);
		int numberOfdays = calendar.getActualMaximum(Calendar.DATE);

		for(int i = 1; i <= numberOfdays; i++) {
			dayList.add(i);
		}

		return dayList;
	}
}
