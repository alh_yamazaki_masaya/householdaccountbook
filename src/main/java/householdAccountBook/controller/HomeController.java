package householdAccountBook.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import householdAccountBook.entity.Month;
import householdAccountBook.form.MonthForm;
import householdAccountBook.form.PaymentForm;
import householdAccountBook.service.HomeService;

@Controller
public class HomeController {

	@Autowired
	HomeService homeService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showHome(Model model) {

		PaymentForm paymentForm = new PaymentForm();
		// DBから家計簿のデータを引っ張てくる
		MonthForm monthForm = new MonthForm();

		List<Month> months = homeService.getMonthList();

		model.addAttribute("months", months);
		model.addAttribute("registerForm", monthForm);
		model.addAttribute("paymentForm", paymentForm);
		return "home";
	}

}
