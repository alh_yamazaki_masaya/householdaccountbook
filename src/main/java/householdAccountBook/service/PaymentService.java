package householdAccountBook.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import householdAccountBook.dto.PaymentDto;
import householdAccountBook.entity.Payment;
import householdAccountBook.form.PaymentForm;
import householdAccountBook.mapper.PaymentMapper;

@Service
public class PaymentService {

	@Autowired
	private PaymentMapper paymentMapper;

	public List<Payment> getPaymentAll(Date month) {

		List<Payment> payments = paymentMapper.getPaymentAll(month);

		return payments;
	}

	public void insertPayment(PaymentForm paymentForm) {

		PaymentDto paymentDto = new PaymentDto();
		Payment payment = new Payment();

		BeanUtils.copyProperties(paymentForm, paymentDto);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
		Date month = new Date();

		String formatMonth = paymentForm.getFormatMonth();
		try {
			month = simpleDateFormat.parse(formatMonth);
		} catch (ParseException e) {

		}

		paymentDto.setMonth(month);

		BeanUtils.copyProperties(paymentDto, payment);

		paymentMapper.insertPayment(payment);
	}

	public void updatePayment(PaymentForm paymentForm) {

		PaymentDto paymentDto = new PaymentDto();
		Payment payment = new Payment();

		BeanUtils.copyProperties(paymentForm, paymentDto);
		BeanUtils.copyProperties(paymentDto, payment);

		paymentMapper.updatePayment(payment);
	}

	public void deletePayment(PaymentForm paymentForm) {

		int id = paymentForm.getId();
		paymentMapper.deletePayment(id);
	}
}
