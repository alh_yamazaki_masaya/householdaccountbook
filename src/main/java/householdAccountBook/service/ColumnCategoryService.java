package householdAccountBook.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import householdAccountBook.entity.ColumnCategory;
import householdAccountBook.mapper.ColumnCategoryMapper;

@Service
public class ColumnCategoryService {

	@Autowired
	private ColumnCategoryMapper mapper;

	public Map<Integer, String> getCategoryAll() {

		List<ColumnCategory> categories = mapper.getCategoryAll();

		Map<Integer, String> categoryMap = new HashMap<Integer, String>();

		for (ColumnCategory category : categories) {
			categoryMap.put(category.getId(), category.getName());
		}

		return categoryMap;
	}
}
