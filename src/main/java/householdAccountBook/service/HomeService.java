package householdAccountBook.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import householdAccountBook.entity.Month;
import householdAccountBook.mapper.MonthMapper;

@Service
public class HomeService {

	@Autowired
	private MonthMapper monthMapper;

	public List<Month> getMonthList() {

		List<Month> months = monthMapper.getMonthist();

		return months;
	}

	public Month getMonth(Date date) {

		Month month = monthMapper.getMonth(date);

		return month;
	}
}
