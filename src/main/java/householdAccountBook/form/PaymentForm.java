package householdAccountBook.form;

import java.io.Serializable;

public class PaymentForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String month;

	private String day;

	private int categoryId;

	private int payment;

	private String memo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getPayment() {
		return payment;
	}

	public void setPayment(int payment) {
		this.payment = payment;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getFormatMonth() {

		return (month.substring(0, 4) + '-' + month.substring(4, 6));
	}

}