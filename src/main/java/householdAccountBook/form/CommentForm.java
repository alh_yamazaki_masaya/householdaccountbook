package householdAccountBook.form;

import java.io.Serializable;

public class CommentForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private int commentId;
	private String comment;
	private int messageId;

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getMessageId() {
		return messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

}