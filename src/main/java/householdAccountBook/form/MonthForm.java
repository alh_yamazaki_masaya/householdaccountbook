package householdAccountBook.form;

import java.io.Serializable;

public class MonthForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String month;



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}
}